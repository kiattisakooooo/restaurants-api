# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])

for i in 1..5
  Restaurant.create(name: "Restaurant #{i}")
end

for i in 1..10 do
  Dish.create(name: "Dish #{i}", restaurant_ids: Restaurant.all.map(&:id))
end
