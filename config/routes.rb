Rails.application.routes.draw do
  root 'restaurants#index'

  resources :restaurants do
    resources :dishes, only: :index
  end

  resources :dishes do
    resources :restaurants, only: :index
  end
end
