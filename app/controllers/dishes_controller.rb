class DishesController < ApplicationController
  before_action :set_restaurant, only: :index
  before_action :set_dish, only: [:show, :update, :destroy]

def index
    render json: @restaurant ? @restaurant.dishes : Dish.all
  end

  def create
    render json: Dish.create!(dish_params)
  end

  def show
    render json: @dish
  end

  def update
    @dish.update!(dish_params)

    render json: @dish
  end

  def destroy
    render json: @dish.destroy!
  end

  private def set_dish
    @dish = Dish.find(params[:id])
  end

  private def set_restaurant
    @restaurant = Restaurant.find(params[:restaurant_id]) if params[:restaurant_id]
  end

  private def dish_params
    params.permit(
      :name,
      restaurant_ids: []
    )
  end
end
