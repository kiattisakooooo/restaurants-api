class CreateJoinRestaurantsDishes < ActiveRecord::Migration[5.2]
  def change
    create_join_table :restaurants, :dishes do |t|
      t.index [:restaurant_id, :dish_id]
    end
  end
end
