class RestaurantsController < ApplicationController
  before_action :set_dish, only: :index
  before_action :set_restaurant, only: [:show, :update, :destroy]

  def index
    render json: @dish ? @dish.restaurants : Restaurant.all
  end

  def create
    render json: Restaurant.create!(restaurant_params)
  end

  def show
    render json: @restaurant
  end

  def update
    @restaurant.update!(restaurant_params)

    render json: @restaurant
  end

  def destroy
    render json: @restaurant.destroy!
  end

  private def set_restaurant
    @restaurant = Restaurant.find(params[:id])
  end

  private def set_dish
    @dish = Dish.find(params[:dish_id]) if params[:dish_id]
  end

  private def restaurant_params
    params.permit(
      :name,
      dish_ids: []
    )
  end
end
